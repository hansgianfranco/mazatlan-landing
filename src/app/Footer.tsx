"use client";

import Image from "next/image";
import Link from "next/link";

const terms = {
  title_es: "Lorem Ipsum",
  content_es: "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham."
}

const socialNetworks = [
  { 
    img: "/commons/x.svg", 
    link: "https://twitter.com/MazatlanFC" 
  },
  {
    img: "/commons/fb.svg",
    link: "https://www.facebook.com/MazatlanFC",
  },
  {
    img: "/commons/instagram.svg",
    link: "https://www.instagram.com/mazatlanfc",
  },
  {
    img: "/commons/youtube.svg",
    link: "https://www.youtube.com/mazatlanfc",
  },
  {
    img: "/commons/tiktok.svg",
    link: "https://www.tiktok.com/@mazatlanfc?_t=8kIHa7Pj10w&_r=1",
  },
  {
    img: "/commons/linkedin.svg",
    link: "https://www.linkedin.com/company/mazatlanfc",
  },
];

export const Footer: React.FC<{ showOverlay: React.Dispatch<React.SetStateAction<boolean>>, selectedNews: React.Dispatch<React.SetStateAction<any>> }> = ({ showOverlay, selectedNews }) => {
  return (
    <div className="pt-[32px] md:pt-[98px] pb-[70px] md:pb-[35px] md:px-[30px] border-t border-[#3c3c3c]">
      <div className="w-full max-w-[1200px] mx-auto">
      <div className="w-full max-w-[1200px] mx-auto">
        <div className="border-b-[20px] px-[30px] md:px-0 border-b-white pb-8">
          <div className="md:flex items-center justify-between max-w-[314px] md:max-w-full mx-auto md:mx-0">

            <div className="relative max-w-[320px] md:max-w-[518px] lg:max-w-full mb-[30px] md:mb-0 mx-auto md:mx-0">
              <Image
                fill
                sizes="100%"
                alt="donwload"
                className="!relative"
                src="/newkit/title.png"
                style={{
                  objectFit: "contain",
                }}
              />
            </div>

            <div className="flex justify-center flex-wrap md:justify-start gap-[22px] lg:gap-[20px]">
              {socialNetworks.map((socialNetwork) => {
                return (
                  <Link
                    passHref
                    href={socialNetwork.link}
                    key={socialNetwork.img}
                  >
                    <Image
                      width={24}
                      height={24}
                      alt="donwload"
                      src={socialNetwork.img}
                      style={{
                        objectFit: "contain",
                      }}
                    />
                  </Link>
                );
              })}
            </div>
          </div>
        </div>
      </div>
      <div className="md:flex md:justify-end px-[30px] md:px-0 md:gap-[50px] pt-[15px] md:pt-[35px]">
      <div className="text-sm font-chakrapetch text-[#ABAAAA] my-2 mb-[32px] text-center md:hidden">Copyright © 2023 Operadora de Escenarios Deportivos</div>
        <div 
          className="text-sm cursor-pointer font-chakrapetch font-semibold text-[#ABAAAA] my-2"
          onClick={() => {
            selectedNews(terms);
            showOverlay(true);
          }}
        >
          Términos y condiciones
        </div>
        <div 
          className="text-sm cursor-pointer font-chakrapetch font-semibold text-[#ABAAAA] my-2"
          onClick={() => {
            selectedNews(terms);
            showOverlay(true);
          }}
        >
          Privacidad
        </div>
        <div 
          className="text-sm cursor-pointer font-chakrapetch font-semibold text-[#ABAAAA] my-2"
          onClick={() => {
            selectedNews(terms);
            showOverlay(true);
          }}
        >Cookies</div>
        <div className="hidden text-sm font-semibold text-[#ABAAAA] my-2 md:block">Copyright © 2020 — 2023 Mazatlán F.C.</div>
      </div>
      </div>
    </div>
  );
};
