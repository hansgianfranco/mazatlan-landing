"use client";

import { useState } from "react";
import {LogoBanners} from "@/app/LogosBanner";
import {Nav} from "@/app/Nav";
import {Header} from "@/app/Header";
import {Carousel} from "@/app/Carousel";
import {Matches} from "@/app/Matches";
import Image from "next/image";
import {DownloadApp} from "@/app/DownloadApp";
import {NewKit} from "@/app/NewKit";
import {Shop} from "@/app/Shop";
import {ManTeam} from "@/app/ManTeam";
import {WomanTeam} from "@/app/WomanTeam";
import {Footer} from "@/app/Footer";
import { MazaPlus } from "./MazaPlus";
import {Promotions} from "@/app/Promotions";
import {OverlayMatch} from "@/app/OverlayMatch";
import {OverlayMember} from "@/app/OverlayMember";
import {OverlayNews} from "@/app/OverlayNews";

import Link from "next/link";

export default function Home() {
  const [showMatchOverlay, setShowMatchOverlay] = useState(false);
  const [showMemberOverlay, setShowMemberOverlay] = useState(false);
  const [showNewsOverlay, setShowNewsOverlay] = useState(false);

  const [selectedMatch, setSelectedMatch] = useState(null);
  const [selectedMember, setSelectedMember] = useState(null);
  const [selectedNews, setSelectedNews] = useState(null);


  const closeMatchOverlay = () => {
    setShowMatchOverlay(false);
    setSelectedMatch(null);
    const body = document.body;
    body.classList.remove('modal-open');
  };

  const closeMemberOverlay = () => {
    setShowMemberOverlay(false);
    setSelectedMember(null);
    const body = document.body;
    body.classList.remove('modal-open');
  };

  const closeNewsOverlay = () => {
    setShowNewsOverlay(false);
    setSelectedNews(null);
    const body = document.body;
    body.classList.remove('modal-open');
  };

  return (
    <main className="relative bg-black min-h-[100vh]">
      <div className="fixed flex flex-col z-50 top-0 left-0 right-0">
        <LogoBanners />
        <Nav />
      </div>
      <Header />
      <Carousel showOverlay={setShowNewsOverlay} selectedNews={setSelectedNews} />
      <Matches showOverlay={setShowMatchOverlay} selectedMatch={setSelectedMatch} />
      <Promotions />
      <DownloadApp />
      <Promotions />
      <NewKit />
      <Shop />
      <MazaPlus />
      <ManTeam showOverlay={setShowMemberOverlay} selectedMember={setSelectedMember} />
      <WomanTeam />
      {showMatchOverlay && <OverlayMatch match={selectedMatch} closeOverlay={closeMatchOverlay} />}
      {showMemberOverlay && <OverlayMember member={selectedMember} closeOverlay={closeMemberOverlay} />}
      {showNewsOverlay && <OverlayNews news={selectedNews} closeOverlay={closeNewsOverlay} />}
      <Footer showOverlay={setShowNewsOverlay} selectedNews={setSelectedNews} />
    </main>
  );
}
