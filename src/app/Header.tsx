import { useEffect, useRef, useState } from "react";
import { IoPlayCircle, IoPauseCircle } from "react-icons/io5";

export const Header: React.FC = () => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const [isPlaying, setIsPlaying] = useState(false);

  const handlePlay = () => {
    if (videoRef.current) {
      videoRef.current.muted = false;
      videoRef.current.play()
        .then(() => {
          setIsPlaying(true);
        })
        .catch((error) => {
          console.error("Error playing video:", error);
        });
    }
  };

  const handlePause = () => {
    if (videoRef.current) {
      videoRef.current.pause();
      setIsPlaying(false);
    }
  };

  useEffect(() => {
    const handleAutoPlay = () => {
      if (videoRef.current) {
        videoRef.current.autoplay = true;
        videoRef.current.muted = true;
      }
    };

    handleAutoPlay();

    const handleClick = () => {
      if (typeof window !== 'undefined' && window.innerWidth <= 992 && isPlaying) {
        handlePause();
      }
    };

    document.addEventListener('click', handleClick);

    return () => {
      document.removeEventListener('click', handleClick);
    };
  }, [isPlaying]);

  return (
    <div className="mt-[110px]">
      <div className="relative overflow-hidden h-[375px] md:h-[680px] after:absolute after:w-full after:h-full after:top-0 after:bottom-0 after:left-0 after:right-0 after:bg-gradient-to-b after:from-transparent after:to-black/[.2]">
        <video
          ref={videoRef}
          width="400"
          height="300"
          className="absolute top-0 left-0 bottom-0 right-0 min-w-full min-h-full object-cover"
          autoPlay={true}
          muted
          poster="/headers/header1.jpeg"
        >
          <source src="https://firebasestorage.googleapis.com/v0/b/mazatlan-f3165.appspot.com/o/Video%20app.mp4?alt=media&token=3f087490-d8e7-4c98-a9da-391ab289ab62" type="video/mp4" />
        </video>
        <div className="absolute opacity-0 hover:opacity-100 transition-all bottom-0 left-0 right-0 top-0 flex justify-center items-center z-10">
          {typeof window !== 'undefined' && window.innerWidth <= 992 && (
            <>
              {isPlaying && (
                <button onClick={handlePause} className="w-[70px] h-[70px]">
                  <IoPauseCircle className="text-[70px]" />
                </button>
              )}
              {!isPlaying && (
                <button onClick={handlePlay} className="w-[70px] h-[70px]">
                  <IoPlayCircle className="text-[70px]" />
                </button>
              )}
            </>
          )}
        </div>
      </div>
    </div>
  );
};