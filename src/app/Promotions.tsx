"use client";

import Image from "next/image";
import useSWR from "swr";
import type {img} from "@/constants";
import {apiUrl, fetcher} from "@/constants";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./custom-slick.css";

export const Promotions: React.FC = () => {

  const slickSettings = {
    dots: false,
    arrows: false,
    infinite: false,
    speed: 500,
    autoplay: true,
    autoplayspeed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  }

  const { data, error, isLoading } = useSWR(
    `${apiUrl}/api/promotions`,
    fetcher,
  );

  if (error) return null;
  if (isLoading) return "";

  return (
    <div className="relative px-[10px] md:px-[30px] bg-black mb-[45px] md:mb-[120px]">
      <div className="max-w-[1200px] mx-auto ">
      <Slider {...slickSettings}>
        {data?.data?.map((img: img) => (
          <a key={img.id} href={img.web_view_url} className="relative w-full overflow-hidden rounded-[10px] mx-auto">
            <Image
              fill
              key={img.id}
              alt="carousels"
              className="block !h-auto !relative"
              src={img.image_url}
            />
          </a>
        ))}
      </Slider>
    </div>
    </div>
  );
};
