"use client";

import useSWR from "swr";
import Image from "next/image";
import type { carousel } from "@/constants";
import Slider from "react-slick";
import { apiUrl, fetcher } from "@/constants";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./custom-slick.css";

export const Carousel: React.FC<{ showOverlay: React.Dispatch<React.SetStateAction<boolean>>, selectedNews: React.Dispatch<React.SetStateAction<any>> }> = ({ showOverlay, selectedNews }) => {

  const slickSettings = {
    dots: false,
    arrows: false,
    infinite: false,
    variableWidth: true,
    speed: 500,
    slidesToScroll: 1
  }

  const { data, error, isLoading } = useSWR(`${apiUrl}/api/news`, fetcher);

  if (error) return null;
  if (isLoading) return "";

  return (
    <div className="relative pt-[30px] md:pt-0 md:top-[-4rem] mb-[13px] md:mb-[72px] overflow-hidden">
      <div className="slick-custom-1">
        <Slider {...slickSettings}>
          {data.data.map((carousel: carousel) => {
            return (
              <div key={carousel.id} className="px-[10px] md:px-[20px]">
                <div className="relative overflow-hidden h-[193px] w-[250px] hover:w-[250px] md:h-[320px] md:hover:w-[420px] md:w-[200px] rounded-2xl duration-300">
                  <div className="absolute bottom-0 top-0 left-0 right-0 rounded-[16px] z-[10] h-[193px] md:h-[323px] after:h-[323px] after:absolute after:bottom-0 after:left-0 after:right-0 after:bg-gradient-to-b after:from-transparent after:to-black"/>
                  <Image
                    fill
                    sizes="80vw"
                    alt="carousels"
                    src={carousel.image_url}
                    style={{
                      objectFit: "cover",
                      borderRadius: "16px",
                    }}
                  />
                  <div className="absolute uppercase font-bold bottom-[4rem] left-[1rem] text-[10px] md:text-xl z-[10]">
                    {carousel.title_es}
                  </div>
                  <button 
                    className="absolute bottom-[1rem] font-semibold left-[1rem] text-[8px] md:text-base bg-[#FF2850] px-4 py-2 z-[10]"
                    onClick={() => {
                      selectedNews(carousel);
                      showOverlay(true);
                    }}
                  >
                    VER CONTENIDO
                  </button>
                </div>
              </div>
            );
          })}
        </Slider>
      </div>
    </div>
  );
};
