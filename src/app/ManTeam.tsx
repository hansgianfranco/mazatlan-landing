"use client";

import Image from "next/image";
import useSWR from "swr";
import type {member} from "@/constants";
import {apiUrl, fetcher} from "@/constants";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./custom-slick.css";

export const ManTeam: React.FC<{ showOverlay: React.Dispatch<React.SetStateAction<boolean>>, selectedMember: React.Dispatch<React.SetStateAction<any>> }> = ({ showOverlay, selectedMember }) => {
  const slickSettings = {
    dots: true,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1
        }
      },
    ]
  }

  const { data, error, isLoading } = useSWR(`${apiUrl}/api/team/members?lang=ES`, fetcher);

  if (error) return null;
  if (isLoading) return "";

  return (
    <div id="equipos" className="relative mb-[60px] md:mb-[80px]">
      <div className="relative px-[30px] pb-[70px] w-full z-[10] md:before:absolute md:before:h-auto md:before:z-10 md:before:w-[1px] md:before:bg-white md:before:left-[30px] md:before:bottom-[48px] md:before:top-[20px]">
        <div className="relative text-[20px] md:text-[34px] py-[30px] font-bold text-center w-fit cursor-pointer md:ml-[4rem] z-[11]">
          PRIMER EQUIPO VARONIL
        </div>
        <div className="slick-custom-1 relative md:px-[50px] z-[11]">
          <Slider {...slickSettings}>
            {data?.data?.map((member: member) => (
              <button 
                key={member.id}
                className="relative px-[10px] h-[244px] md:h-[320px] lg:h-[471px] overflow-hidden"
                onClick={() => {
                  selectedMember(member);
                  showOverlay(true);
                }}
              >
                <div className="absolute w-auto h-full top-0 left-[10px] right-[10px]">
                <Image
                  fill
                  sizes="100%"
                  alt="donwload"
                  src={member.image_url ? member.image_url : "/players/man-player-default.png"}
                  style={{
                    objectFit: "cover",
                  }}
                />
                </div>
                <div className="absolute bottom-[24px] left-0 right-0 mx-auto text-center">
                  <p className="text-[21px] md:text-[28px] lg:text-[40px]  mb-[5px]">{ (member.shirt ?? "") + " " + member.name + " " + member.last_name}</p>
                  <p className="text-[14px] ">{ member.position }</p>
                </div>
              </button>
            ))}
          </Slider>
        </div>

        <Image
          fill
          sizes="100%"
          alt="donwload"
          src="/players/bg-man-team.jpeg"
          style={{
            objectFit: "cover",
          }}
        />
      </div>
    </div>
  );
};
