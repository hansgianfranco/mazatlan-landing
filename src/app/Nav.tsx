import Image from "next/image";
import { useState } from "react";

export const Nav: React.FC = () => {

  const [openMenu, setOpenMenu] = useState(false)

  const handleLinkClick = (targetId: string) => {
    const targetElement = document.getElementById(targetId);
    if (targetElement) {
      const offsetTop = targetElement.offsetTop;
      window.scrollTo({
        top: offsetTop,
        behavior: 'smooth'
      });
      setOpenMenu(false)
    }
  };

  const handleOpenMenu = () => {
    setOpenMenu(!openMenu)
    const body = document.body;
    if (openMenu) {
      body.classList.remove('modal-open');
    } else {
      body.classList.add('modal-open');
    }
  }

  return (
    <>
      <div className="order-1 md:order-2 w-full h-[58px] md:h-[77px] bg-[#331E4E]">
        <div className="max-w-[1280px] container mx-auto px-[30px] py-[20px] md:py-0 flex items-center md:items-start justify-between">
          <div className="w-[158px] h-[19px] md:w-[90px] md:h-[89.93px] xl:ml-[50px] md:mt-2 relative z-10">
            <Image className="hidden md:block" fill sizes="80vw" alt="" src="/logos/nav-logo.svg" />
            <Image className="block md:hidden" fill sizes="100%" alt="" src="/logos/logo-white.svg" />
          </div>
          <div className="hidden md:flex item-center gap-6 xl:gap-8 h-[32px] mt-[22px]">
            <a href="https://boletomovil.com/mazatlanfc" target="_blank" className="bg-cyan-500 text-base font-bold leading-[26px] py-[2px] px-[11px] cursor-pointer">
              BOLETOS
            </a>
            <a href="https://mazashop.mx" target="_blank" className="bg-cyan-500 text-base font-bold leading-[26px] py-[2px] px-[11px] cursor-pointer">
              TIENDA
            </a>
            <a onClick={() => handleLinkClick('proximos_partidos')} href="#proximos_partidos" className="text-base font-bold leading-[32px] cursor-pointer">PRÓXIMOS PARTIDOS</a>
            <a href="https://www.youtube.com/mazatlanfc" target="_blank" className="text-base font-bold leading-[32px] cursor-pointer">MAZA PLUS</a>
            <a onClick={() => handleLinkClick('equipos')} href="#equipos" className="text-base font-bold leading-[32px] cursor-pointer">EQUIPOS</a>
          </div>
          <button onClick={() => handleOpenMenu()} className="block w-[25px] h-[25px] relative z-[10] md:hidden">
            <Image fill sizes="100%" alt="" src="/icons/icon-menu.svg" />
          </button>
        </div>
      </div>
      <div className={`fixed top-0 left-0 right-0 bottom-0 w-full h-full bg-black/[.8] z-[100] ${ openMenu ? 'block' : 'hidden'}`}>
        <div className={`absolute md:hidden w-[272px] bg-gradient-to-b from-[#682FA5] to-[#1A0B29] rounded-tl-[12px] flex flex-col justify-between pt-[40px] pb-[35px] rounded-bl-[12px] top-0 bottom-0 right-0 h-full z-[100]`}>
            <button onClick={() => handleOpenMenu()}  className="absolute left-[18px] top-[50px] block w-[24px] min-h-[24px] h-[24px] relative z-[10]">
              <Image fill sizes="100%" alt="" src="/icons/icon-left.svg" />
            </button>
            <div className="relative w-[123px] h-[123px] mx-auto mb-[20px] z-[2]">
              <Image className="" fill sizes="100%" alt="" src="/logos/nav-logo.svg" />
            </div>
            <nav className="px-[15px] relative z-[2]">
              <a href="https://boletomovil.com/mazatlanfc" target="_blank" className="block text-[16px] font-bold px-[15px] py-[20px] border-b leading-[32px] cursor-pointer">
                BOLETOS
              </a>
              <a href="https://mazashop.mx" target="_blank" className="block text-[16px] px-[15px] py-[20px] font-bold leading-[32px] border-b cursor-pointer">
                TIENDA
              </a>
              <a onClick={() => handleLinkClick('proximos_partidos')} href="#proximos_partidos" className="block text-[16px] px-[15px] py-[20px] font-bold leading-[32px] border-b cursor-pointer">PRÓXIMOS PARTIDOS</a>
              <a href="https://www.youtube.com/mazatlanfc" target="_blank" className="block text-[16px] px-[15px] py-[20px] font-bold leading-[32px] border-b cursor-pointer">MAZA PLUS</a>
              <a onClick={() => handleLinkClick('equipos')} href="#equipos" className="block text-[16px] px-[15px] py-[20px] font-bold leading-[32px] border-b cursor-pointer">EQUIPOS</a>
            </nav>
            <p className="relative font-light text-[12px] px-[30px] text-center leanding-[14px] z-[2]">Mazatlán FC ©. Todos los derechos reservados. 2023</p>
            <div className="absolute w-full h-[563px] top-[60px] right-0 z-[1] opacity-5">
              <Image fill sizes="100%" alt="" src="/headers/sidebar-logo.svg" />
            </div>
        </div>
      </div>
      
    </>
  );
};
