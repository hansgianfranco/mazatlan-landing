"use client";

import Image from "next/image";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./custom-slick.css";

export const Shop: React.FC = () => {
  const slickSettings = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  }
  return (
    <div className="relative after:w-[202px] mb-[22px] md:mb-[120px]">
      <div className="relative h-[450px] md:h-[630px] py-[20px] md:pt-[70px] w-full after:bg-gradient-to-b after:absolute after:z-[10] after:h-[202px] after:w-full after:from-transparent after:to-black after:bottom-0 after:left-0 after:right-0">
        <div className="md:flex md:items-center h-[338px] md:h-[480px]">
          <div className="w-full md:w-1/3 px-[30px]">
            <div className="relative h-[73px] max-w-[213px] md:h-[120px] mb-[8px] md:mb-[40px] md:max-w-[350px] cursor-pointer mx-auto z-[10]">
              <Image
                fill
                sizes="100vw"
                alt="donwload"
                src="/shop/logo.png"
                style={{
                  objectFit: "contain",
                }}
              />
            </div>
            <div className="relative text-[18px] md:text-[34px] w-full md:max-w-[320px] mx-auto mb-[10px] md:mb-0 font-bold text-center md:w-fit uppercase leading-10 cursor-pointer z-[10]">
              DESCUBRE LO QUE
              HAY PARA TI
            </div>
          </div>

          <div className="relative slick-custom-2 h-[211px] w-full md:h-[480px] md:w-2/3 max-w-[960px] z-[20]">
            <Slider {...slickSettings}>
              <div className="w-[422px] h-[211px] md:h-[480px] md:w-[960px] ">
                <div className="flex justify-center">
                  <div className="relative w-[211px] h-[211px] md:w-[480px] md:h-[480px]">
                    <Image
                      fill
                      sizes="100%"
                      alt="donwload"
                      className="md:float-left"
                      src="/shop/t-shirt.jpeg"
                      style={{
                        objectFit: "cover",
                      }}
                    />
                  </div>
                  <div className="relative w-[211px] h-[211px] md:w-[480px] md:h-[480px]">
                    <Image
                      fill
                      sizes="100%"
                      alt="donwload"
                      className="md:float-left"
                      src="/shop/t-shirt-man.jpeg"
                      style={{
                        objectFit: "cover",
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="w-[422px] h-[211px] md:h-[480px] md:w-[960px] ">
                <div className="flex justify-center">
                  <div className="relative w-[211px] h-[211px] md:w-[480px] md:h-[480px]">
                    <Image
                      fill
                      sizes="100%"
                      alt="donwload"
                      className="md:float-left"
                      src="/shop/t-shirt.jpeg"
                      style={{
                        objectFit: "cover",
                      }}
                    />
                  </div>
                  <div className="relative w-[211px] h-[211px] md:w-[480px] md:h-[480px]">
                    <Image
                      fill
                      sizes="100%"
                      alt="donwload"
                      className="md:float-left"
                      src="/shop/t-shirt-man.jpeg"
                      style={{
                        objectFit: "cover",
                      }}
                    />
                  </div>
                </div>
              </div>
            </Slider>
          </div>
        </div>

        
        <div className="relative py-[20px] px-[20px] md:px-0 md:py-0 z-[20] w-[300px] md:mt-[40px] ml-auto mr-0 flex justify-end md:justify-start">
          <a href="https://mazashop.mx" target="_blank" className="text-sm border-2 border-t-[#FF2850] font-bold border-b-[#FF2850] border-x-0 px-4 py-2 z-[10]">
            IR A LA TIENDA
          </a>
        </div>

        <Image
          fill
          sizes="100vw"
          alt="donwload"
          src="/shop/shop.png"
          style={{
            objectFit: "cover",
            objectPosition: "top center"
          }}
        />
      </div>
    </div>
  );
};
